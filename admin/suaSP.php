<?php
require 'header.php';
				$id = $_GET['id'];
				settype($id, 'int');
				$data = "select * from bh_sanpham  where id= '$id'";

                $qr_pl = "select * from bh_phanloai";
                $pl = mysqli_query($conn, $qr_pl);

				$sanpham = mysqli_query($conn, $data);
				$row = mysqli_fetch_assoc($sanpham);

				if (isset($_POST['btnSua'])){
					$tensp = $_POST['tensp'];
					$code = changeTitle($tensp);
					$giasp = $_POST['giasp'];
					$giagoc = $_POST['giagoc'];
					$xuatxu = $_POST['xuatxu'];
					$trongluong = $_POST['trongluong'];
					$nhanhieu = $_POST['nhanhieu'];
					$mota = $_POST['mota'];
					$khuyenmai = $_POST['khuyenmai'];
					$phanloai = $_POST['phanloai'];


					if ($_FILES['anhsp1']['name'] != null ){
					    unlink('../uploads/'.$row['anhsp']);
					    $now = date('d-m-y');
						$anhsp_name = $_FILES['anhsp1']['name'];
						$namesp_mh = changeTitle($anhsp_name);
						$anhsp = $now.'-sp-'.$namesp_mh;
						$anhsp_tmp = $_FILES['anhsp1']['tmp_name'];
						move_uploaded_file($anhsp_tmp,"../uploads/".$anhsp);
						mysqli_query($conn,"update bh_sanpham set anhsp = '$anhsp' where id = '$id'");
					}
					if ($_FILES['anhsp2']['name'] != null ){
						unlink('../uploads/'.$row['anh2']);
						$now = date('d-m-y');
						$anh2_name = $_FILES['anhsp2']['name'];
						$name2_mh = changeTitle($anh2_name);
						$anh2 = $now.'-sp-'.$name2_mh;
						$anh2_tmp = $_FILES['anhsp2']['tmp_name'];
						move_uploaded_file($anh2_tmp,"../uploads/".$anh2);
						mysqli_query($conn,"update bh_sanpham set anh2 = '$anh2' where id = '$id'");
					}
					if ($_FILES['anhsp3']['name'] != null ){
						unlink('../uploads/'.$row['anh3']);
						$now = date('d-m-y');
						$anh3_name = $_FILES['anhsp3']['name'];
						$name3_mh = changeTitle($anh3_name);
						$anh3 = $now.'-sp-'.$name3_mh;
						$anh3_tmp = $_FILES['anhsp3']['tmp_name'];
						move_uploaded_file($anh3_tmp,"../uploads/".$anh3);
						mysqli_query($conn,"update bh_sanpham set anh3 = '$anh3' where id = '$id'");
					}

					$qr = "update bh_sanpham set  name = '$tensp', code = '$code', gia = '$giasp', giacanhtranh = '$giagoc', mota = '$mota', hangsx = '$nhanhieu', xuatxu = '$xuatxu', trongluong = '$trongluong', khuyenmai = '$khuyenmai', id_phanloai = '$phanloai' where id = '$id'";
					mysqli_query($conn, $qr);
					mysqli_free_result($qr);
					mysqli_free_result($qr_pl);
					mysqli_close($conn);
					header('location: danhsachSP.php');
				}
				?>
                <form method="post" enctype="multipart/form-data" style="width: 900px; margin: 0 auto">
                    <h3>Sửa sản phẩm</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" value="<?= $row['name'] ?>" name="tensp" class="form-control" placeholder="Tên sản phẩm">
                            </div>
                            <div class="form-group">
                                <input type="text" value="<?= $row['gia'] ?>" name="giasp" class="form-control" placeholder="Giá bán ra">
                            </div>
                            <div class="form-group">
                                <input type="text" value="<?= $row['giacanhtranh'] ?>" name="giagoc" class="form-control" placeholder="Giá gốc của sản phẩm">
                            </div>
                            <div class="form-group">
                                <input type="text" value="<?= $row['xuatxu'] ?>" name="xuatxu" class="form-control" placeholder="Xuất xứ">
                            </div>
                            <div class="form-group">
                                <input type="text" value="<?= $row['hangsx'] ?>" name="nhanhieu" class="form-control" placeholder="Nhãn hiệu">
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="khuyenmai">
                                    <option value="0" <?php if ($row['khuyenmai'] == 0) echo "selected='selected'"?>>Không khuyến mại</option>
                                    <option value="1" <?php if ($row['khuyenmai'] == 1) echo "selected='selected'"?>>Có khuyến mại</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" value="<?= $row['trongluong'] ?>" name="trongluong" class="form-control" placeholder="Trọng lượng của sản phẩm (g)">
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="phanloai" >
			                        <?php while ($row_pl = mysqli_fetch_assoc($pl)) {?>
                                        <option <?php if ($row_pl['id'] == $row['id_phanloai']) echo "selected='selected'" ?> value="<?= $row_pl['id'] ?>"><?= $row_pl['name'] ?></option>
			                        <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mô tả ngắn gọn</label>
                                <textarea class="form-control" rows="7" name="mota"><?= $row['mota'] ?></textarea>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputFile">Hình ảnh sản phẩm</label>
                                <p style="font-size: 12px; font-style: italic">(Ảnh đầu sẽ là ảnh đại diện của sản phẩm)</p>
                                <input type="file" name="anhsp1" class="form-control" style="margin-bottom: 35px">
                                <input type="file" name="anhsp2" class="form-control" style="margin-bottom: 35px">
                                <input type="file" name="anhsp3" class="form-control" style="margin-bottom: 35px">
                                <div class="row">
                                    <div class="col-md-4"><p>Ảnh chính</p><img style="width: 124px; max-height: 150px" src="../uploads/<?= $row['anhsp'] ?>"></div>
                                    <div class="col-md-4"><p>Ảnh 2</p><img style="width: 124px; max-height: 150px" src="../uploads/<?= $row['anh2'] ?>"></div>
                                    <div class="col-md-4"><p>Ảnh 3</p><img style="width: 124px; max-height: 150px" src="../uploads/<?= $row['anh3'] ?>"></div>
                                </div>


                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="submit" name="btnSua" class="btn btn-primary" value="Cập nhật">
                        </div>

                    </div>

                </form>
                <br>
<?php require 'footer.php'?>
