<?php require 'header.php'?>
				<div class="create" style="margin-left: 275px">
                    <?php
                        if (isset($_POST['btnThemPL'])){
                            $phanloai = $_POST['tenpl'];
                            $code = changeTitle($phanloai);
                            $pl = "insert into bh_phanloai values (null, '$phanloai', '$code')";
                            mysqli_query($conn, $pl);
                            header('location: phanloaiSP.php');
                        }
                    ?>
                    <form method="post" style="width: 500px;">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="tenpl" class="form-control" placeholder="Tên phân loại">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <input type="submit" name="btnThemPL" class="btn btn-success" value="Thêm mới">
                            </div>

                        </div>
                    </form>
				</div>
				<br>
				<table class="table table-bordered table-condensed" style="width: 700px; margin: 0 auto">
					<thead>
					<th style="text-align: center">STT</th>
					<th style="text-align: left">Tên loại sản phẩm</th>
					<th style="text-align: center">Sửa</th>
					<th style="text-align: center">Xóa</th>
					</thead>

					<tbody>
					<?php
					$qr = "select * from bh_phanloai order by id desc ";
					$pl= mysqli_query($conn, $qr);
					$stt = 0;
					while ($row = mysqli_fetch_assoc($pl)) {
						$stt++;
                        $idpl = $row['id'];
						?>

						<tr>
							<td style="width: 5%; text-align: center"><?= $stt ?></td>
							<td style="width: 75%; text-align: left"><?= $row["name"]; ?></td>
							<td style="width: 10%; text-align: center"><a href="suaPL.php?id=<?= $row['id'] ?>" style="color: #28a745;"><i class="fa fa-pencil"></i></a></td>
							<td style="width: 10%; text-align: center"><a onclick="return confirm('Bạn chắc chắn muốn xóa phân loại sản phẩm này?')" href="xoaPL.php?id=<?= $row['id'] ?>" style="color: #dc3545"><i class="fa fa-trash"></i></a></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
                <?php require 'footer.php'?>
