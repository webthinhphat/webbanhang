<?php
require 'header.php';

				$qr_pl = "select * from bh_phanloai";
				$pl = mysqli_query($conn, $qr_pl);

				if (isset($_POST['btnThem'])){
					$tensp = $_POST['tensp'];
					$code = changeTitle($tensp);
					$giasp = $_POST['giasp'];
					$giagoc = $_POST['giagoc'];
					$xuatxu = $_POST['xuatxu'];
					$trongluong = $_POST['trongluong'];
					$nhanhieu = $_POST['nhanhieu'];
					$mota = $_POST['mota'];
					$khuyenmai = $_POST['khuyenmai'];
					$phanloai = $_POST['phanloai'];


					if (isset($_FILES['anhsp1'])){
						$now = date('d-m-y');
						$anhsp_name = $_FILES['anhsp1']['name'];
						$namesp_mh = changeTitle($anhsp_name);
						$anhsp = $now.'-sp-'.$namesp_mh;
					    $anhsp_tmp = $_FILES['anhsp1']['tmp_name'];
					    move_uploaded_file($anhsp_tmp,"../uploads/".$anhsp);
					}
					if (isset($_FILES['anhsp2'])){
						$now = date('d-m-y');
						$anh2_name = $_FILES['anhsp2']['name'];
						$name2_mh = changeTitle($anh2_name);
						$anh2 = $now.'-sp-'.$name2_mh;
						$anh2_tmp = $_FILES['anhsp2']['tmp_name'];
						move_uploaded_file($anh2_tmp,"../uploads/".$anh2);
					}
					if (isset($_FILES['anhsp3'])){
						$now = date('d-m-y');
						$anh3_name = $_FILES['anhsp3']['name'];
						$name3_mh = changeTitle($anh3_name);
						$anh3 = $now.'-sp-'.$name3_mh;
						$anh3_tmp = $_FILES['anhsp3']['tmp_name'];
						move_uploaded_file($anh3_tmp,"../uploads/".$anh3);
					}

					$qr = "insert into bh_sanpham values (null, '$tensp', '$code', '$anhsp', '$anh2', '$anh3', '$giasp', '$giagoc', '$mota', '$nhanhieu', '$xuatxu', '$trongluong', '$khuyenmai', '$phanloai')";
					mysqli_query($conn, $qr);
					mysqli_free_result($qr);
					mysqli_close($conn);
                    header('location: danhsachSP.php');
				}
				?>
                <form method="post" enctype="multipart/form-data" style="width: 900px; margin: 0 auto">
                    <h3>Thêm sản phẩm</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="tensp" class="form-control" placeholder="Tên sản phẩm (Bắt buộc)">
                            </div>
                            <div class="form-group">
                                <input type="text" name="giasp" class="form-control" placeholder="Giá bán ra (Bắt buộc)">
                            </div>
                            <div class="form-group">
                                <input type="text" name="giagoc" class="form-control" placeholder="Giá gốc của sản phẩm (Bắt buộc)">
                            </div>
                            <div class="form-group">
                                <input type="text" name="xuatxu" class="form-control" placeholder="Xuất xứ">
                            </div>
                            <div class="form-group">
                                <input type="text" name="nhanhieu" class="form-control" placeholder="Nhãn hiệu">
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="khuyenmai">
                                    <option value="0">Không khuyến mại</option>
                                    <option value="1">Có khuyến mại</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" name="trongluong" class="form-control" placeholder="Trọng lượng của sản phẩm (g)">
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="phanloai" >
                                    <?php while ($row_pl = mysqli_fetch_assoc($pl)) {?>
                                    <option value="<?= $row_pl['id'] ?>"><?= $row_pl['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mô tả</label>
                                <textarea class="form-control" rows="7" name="mota"></textarea>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputFile">Hình ảnh sản phẩm</label>
                                <p style="font-size: 12px; font-style: italic">(Ảnh đầu sẽ là ảnh đại diện của sản phẩm)</p>
                                <input type="file" name="anhsp1" class="form-control" style="margin-bottom: 35px">
                                <input type="file" name="anhsp2" class="form-control" style="margin-bottom: 35px">
                                <input type="file" name="anhsp3" class="form-control" style="margin-bottom: 35px">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="submit" name="btnThem" class="btn btn-success" value="Thêm mới">
                        </div>

                    </div>

                </form>
                <br>

       <?php require 'footer.php'?>