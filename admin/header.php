<?php
ob_start();
session_start();
require '../config.php';
require '../API.php';

if (!isset($_SESSION["idUser"]) || ($_SESSION["phanquyen"] != 1)){
	header('location: ../index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta name="description" content="Responsive Admin Template" />
	<meta name="author" content="SmartUniversity" />
	<title>Admin Page</title>
    <link rel="shortcut icon" href="../images/logobq32.png" />
	<!-- google font -->
	<link href="../../../../../https@fonts.googleapis.com/css@family=Poppins_3A300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
	<link href="assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<!--bootstrap -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/summernote/summernote.css" rel="stylesheet">
	<!-- morris chart -->
	<link href="assets/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link rel="stylesheet" href="assets/plugins/material/material.min.css">
	<link rel="stylesheet" href="assets/css/material_style.css">
	<!-- animation -->
	<link href="assets/css/pages/animate_page.css" rel="stylesheet">
	<!-- Template Styles -->
	<link href="assets/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/style.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/theme-color.css" rel="stylesheet" type="text/css" />
	<!-- favicon -->
	<link rel="shortcut icon" href="assets/img/favicon.ico" />


</head>

<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-sidebar-color logo-dark">
<div class="page-wrapper">
	<!-- start header -->
	<div class="page-header navbar navbar-fixed-top">
		<div class="navbar-custom">
			<div class="hor-menu hidden-sm hidden-xs">
				<ul class="nav navbar-nav">
					<li>
						<div class="page-logo">
							<a href="index.php">
								<img style="width: 50px; height: 50px; margin-top: -5px" src="../images/logobq.png">
								<span style="color: #e9658d " class="logo-default" >Beaute Pure Admin</span> </a>
						</div>
						<!-- logo end -->
						<ul class="nav navbar-nav navbar-left in">
							<li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
						</ul>
					</li>
					<li class="mega-menu-dropdown">
						<a href="danhsachSP.php" class="dropdown-toggle"> <i class="fa fa-dropbox"></i>
							Sản phẩm
							<i class="fa fa-angle-down"></i>
							<span class="arrow "></span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<div class="mega-menu-content">
									<div class="row">
										<div class="col-md-12">
											<ul class="mega-menu-submenu">
												<li>
													<a href="danhsachSP.php" class="nav-link "> <span class="title">Danh sách sản phẩm</span></a>
												</li>
												<li>
													<a href="phanloaiSP.php" class="nav-link "> <span class="title">Phân loại sản phẩm</span></a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<li>
						<a href="slider.php" class="dropdown-toggle"> <i class="fa fa-object-group"></i>
							Silder
							<span class="arrow "></span>
						</a>
					</li>
                    <li>
                        <a href="../index.php" style="color: #e9658d" class="dropdown-toggle"> <i style="color: #e9658d" class="fa fa-shopping-bag"></i>
                            Quay lại cửa hàng
                            <span class="arrow "></span>
                        </a>
                    </li>
				</ul>
			</div>
		</div>
	</div>
	<!-- end header -->
	<div class="page-content-wrapper">
		<div class="page-container">
			<div class="page-content">