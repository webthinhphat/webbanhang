<?php
require 'header.php'
?>
                <div class="create">
                    <a href="themSP.php" class="btn btn-success">Thêm mới  <i class="fa fa-plus"></i></a>
                </div>
                <br>
                <table class="table table-bordered table-condensed">
                    <thead>
                    <th style="text-align: center">STT</th>
                    <th style="text-align: center">Ảnh sản phẩm</th>
                    <th style="text-align: center">Tên sản phẩm</th>
                    <th style="text-align: center">Giá bán</th>
                    <th style="text-align: center">Phân loại</th>
                    <th style="text-align: center">Xuất xứ</th>
                    <th style="text-align: center">Hãng SX</th>
                    <th style="text-align: center">Trọng lượng</th>
                    <th style="text-align: center">Sửa</th>
                    <th style="text-align: center">Xóa</th>

                    </thead>

                    <tbody>
					<?php
					$qr = "select * from bh_sanpham order by id desc ";
					$sanpham = mysqli_query($conn, $qr);
					$stt = 0;
					while ($row = mysqli_fetch_assoc($sanpham)) {
						$stt++;
						$id_pl = $row['id_phanloai'];
                        $qr_pl = "select name from bh_phanloai where id = '$id_pl'";
                        $phanloai = mysqli_query($conn, $qr_pl);
                        $row_pl = mysqli_fetch_assoc($phanloai);
						?>

                        <tr>
                            <td style="width: 3%; text-align: center"><?= $stt ?></td>
                            <td style="width: 10%; text-align: center"><a href="../single.php?sp=<?= $row['code'] ?>"><img style="width: 75%;" src="../uploads/<?= $row['anhsp'] ?>"></a></td>
                            <td style="width: 25%; text-align: left"><?= $row["name"]; ?></td>
                            <td style="width: 10%; text-align: right; font-weight: bold"><?php if ($row['gia'] != null) echo number_format($row['gia'],0,',','.').' VNĐ' ?> </td>
                            <td style="width: 10%; text-align: left"><?= $row_pl['name']; ?></td>
                            <td style="width: 10%; text-align: left"><?= $row['xuatxu']; ?></td>
                            <td style="width: 10%; text-align: left"><?= $row['hangsx']; ?></td>
                            <td style="width: 12%; text-align: right"><?php if($row['trongluong'] != null) echo $row['trongluong'].' g'?> </td>
                            <td style="width: 5%; text-align: center"><a href="suaSP.php?id=<?= $row['id'] ?>" style="color: #28a745;"><i class="fa fa-pencil"></i></a></td>
                            <td style="width: 5%; text-align: center"><a onclick="return confirm('Bạn chắc chắn muốn xóa sản phẩm này?')" href="xoaSP.php?id=<?= $row['id'] ?>" style="color: #dc3545"><i class="fa fa-trash"></i></a></td>
                        </tr>
					<?php } ?>
                    </tbody>
                </table>
<?php require 'footer.php'?>