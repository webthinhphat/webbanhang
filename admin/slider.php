<?php
require 'header.php'
?>
<div class="create">
	<a href="themSL.php" class="btn btn-success">Thêm mới  <i class="fa fa-plus"></i></a>
    <p style="color: red">(Chú ý: Chỉ hiện thị 5 slide mới nhất)</p>
</div>
<br>
<table class="table table-bordered table-condensed">
	<thead>
	<th style="text-align: center">STT</th>
	<th style="text-align: center">Ảnh slider</th>
	<th style="text-align: center">Sửa</th>
	<th style="text-align: center">Xóa</th>
	</thead>

	<tbody>
	<?php
	$qr = "select * from bh_slider order by id desc ";
	$slider = mysqli_query($conn, $qr);
	$stt = 0;
	while ($row = mysqli_fetch_assoc($slider)) {
		$stt++;
		?>
		<tr>
			<td style="width: 5%; text-align: center"><?= $stt ?></td>
			<td style="width: 85%; text-align: center"><img style="width: 45%;" src="../uploads/<?= $row['anh'] ?>"></td>
			<td style="width: 5%; text-align: center"><a href="suaSL.php?id=<?= $row['id'] ?>" style="color: #28a745;"><i class="fa fa-pencil"></i></a></td>
			<td style="width: 5%; text-align: center"><a onclick="return confirm('Bạn chắc chắn muốn xóa slide này?')" href="xoaSL.php?id=<?= $row['id'] ?>" style="color: #dc3545"><i class="fa fa-trash"></i></a></td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<?php require 'footer.php'?>
