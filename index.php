<?php require 'header.php'?>

	<!-- BANNER START -->
	<div class="banner">
		<div class="main-banner">
            <?php
                $pr_sl = "select * from bh_slider order by id desc limit 5";
                $slide = mysqli_query($conn, $pr_sl);
                while ($row_sl = mysqli_fetch_assoc($slide)){
            ?>
            <div> <img src="uploads/<?= $row_sl['anh'] ?>"></div>
			<?php } ?>

		</div>
	</div>
	<!-- BANNER END -->

	<!-- FEATURED PRODUCT AREA START -->
	<section class="ptb-95">
		<div class="container">
			<!-- PRODUCT-LISTING MAIN CLASS START -->
			<div class="product-listing">
				<!-- TAB SECTION CONTENT START -->
				<div class="row mlr_-20">
					<div id="items">
						<div id="sanpham" class="tab_content pro_cat">
                            <!-- ALL TAB CONTENT START -->
							<?php
							if (!isset($_GET['pl']))
								require 'sp_chung.php';
							else
								require 'sp_phanloai.php';
							?>
                            <!-- ALL TAB CONTENT ENDS -->

						</div>
					</div>
				</div>
				<!-- TAB SECTION CONTENT ENDS -->
			</div>
			<!-- PRODUCT-LISTING MAIN CLASS START -->
		</div>
	</section>
	<!-- FEATURED PRODUCT AREA ENDS -->

	<?php require 'footer.php'?>