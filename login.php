<form method="post">
	<div class="form-group">
		<label for="uname" style="color: #ff0735">User name:</label>
		<input type="text" name="uname" class="form-control">
	</div>
	<div class="form-group">
		<label for="password" style="color: #ff0735">Password:</label>
		<input type="password" name="pass" class="form-control">
	</div>
	<button type="submit" name="btnLogin" class="btn btn-danger">Log in</button>
</form>