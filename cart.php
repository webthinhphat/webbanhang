<?php
require 'header.php';
if(isset($_SESSION['idUser'])){
$idUser = $_SESSION['idUser'];
 $qr_cart = "select * from bh_cart where id_user = '$idUser'";
 $sp_cart = mysqli_query($conn, $qr_cart);
?>

<section class="ptb-95">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 mb-xs-30">
				<div class="cart-item-table commun-table">
					<div class="table-responsive">
						<?php if ( mysqli_num_rows($sp_cart) == 0 ){ ?>
							<div style="text-align: center">
								<h1>Hiện chưa có sản phẩm nào trong giỏ hàng</h1>
							<div class="mt-30" > <a href="index.php" class="btn-color btn"><span><i class="fa fa-angle-left"></i></span>Tiếp tục mua sắm</a> </div>
							</div>
						<?php } else { ?>
						<table class="table">
							<thead>
							<tr>
								<th style="text-align: center">Ảnh</th>
								<th style="text-align: center">Tên sản phẩm</th>
								<th style="text-align: center">Giá</th>
								<th style="text-align: center">Số lượng</th>
								<th style="text-align: center">Thành tiền</th>
								<th style="text-align: center">Sửa</th>
								<th style="text-align: center">Xóa</th>
							</tr>
							</thead>
							<tbody>
							<?php
							while ($row = mysqli_fetch_assoc($sp_cart)){
								$id_sp = $row['id_sanpham'];
								$sanpham = mysqli_query($conn, "select * from bh_sanpham where id = '$id_sp'");
								$row_sp = mysqli_fetch_assoc($sanpham);
								?>
							<tr>
								<td><a href="product-page.html">
										<div class="product-image"><a href="single.php?sp=<?= $row_sp['code'] ?>"><img src="uploads/<?= $row_sp['anhsp'] ?>"></a></div>
									</a></td>
								<td><div class="product-title"> <a href="single.php?sp=<?= $row_sp['code'] ?>"><?= $row_sp['name'] ?></a> </div></td>
								<td><ul>
										<li>
											<div class="base-price price-box"> <span class="price"><?= number_format($row_sp['gia'], 0,',','.')?> VNĐ</span> </div>
										</li>
									</ul></td>
								<td><?= $row['soluong'] ?></td>
								<td><div class="total-price price-box"> <span class="price"><?= number_format($row_sp['gia'] * $row['soluong'], 0,',','.')?> VNĐ</span> </div></td>
								<td><a href="suaCart.php?id=<?= $row['id'] ?>"><i style="color: #28a745" data-id="100" class="fa fa-pencil"></i></a></td>
								<td><a href="xoaCart.php?id=<?= $row['id'] ?>"><i style="color: #dc3545" class="fa fa-trash cart-remove-item"></i></a></td>
							</tr>
							<?php } ?>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
		<div class="mb-30">
			<div class="row">
				<div class="col-sm-6">
					<div class="mt-30"> <a href="index.php" class="btn-color btn"><span><i class="fa fa-angle-left"></i></span>Tiếp tục mua sắm</a> </div>
				</div>
				<div class="col-sm-6">
					<input type="submit" name="btnUpdate" value="Cập nhật" class="btn-color btn mt-30 right-side float-none-xs">
				</div>
			</div>
		</div>
		<hr>
	</div>
	<?php } ?>
</section>

<?php require 'footer.php'?>
<?php } else header('location: index.php') ?>