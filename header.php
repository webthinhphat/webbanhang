<?php
ob_start();
session_start();
require 'config.php';
require 'API.php';
if  (isset($_POST['btnLogin'])){
	$un = $_POST['uname'];
	$pass = $_POST['pass'];
	$pass = md5($pass);
	$checklogin = "select * from bh_user where username = '$un' and password = '$pass'";
	$user = mysqli_query($conn, $checklogin);
	if(mysqli_num_rows($user) == 1){
		$row_u = mysqli_fetch_assoc($user);
		$_SESSION["idUser"] = $row_u['id'];
		$_SESSION["hoten"] = $row_u['hoten'];
		$_SESSION["phanquyen"] = $row_u['phanquyen'];
	}
}

if  (isset($_POST['btnLogout'])){
	unset($_SESSION['idUser']);
	unset($_SESSION['hoten']);
	unset($_SESSION['phanquyen']);
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Beaute Pure</title>
	<link rel="shortcut icon" href="images/logobq32.png" />

	<!-- SEO Meta -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="distribution" content="global">
	<meta name="revisit-after" content="2 Days">
	<meta name="robots" content="ALL">
	<meta name="rating" content="8 YEARS">
	<meta name="Language" content="en-us">
	<meta name="GOOGLEBOT" content="NOARCHIVE">
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="css/fotorama.css">
	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
	<link rel="stylesheet" type="text/css" href="css/custom.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
	<link rel="shortcut icon" href="images/favicon.png">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.html">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.html">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.html">

	<!-- JS FILES STARTS -->
	<script src="js/jquery-1.12.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/fotorama.js"></script>
	<script src="js/jquery.magnific-popup.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/custom.js"></script>
	<!-- JS FILES END -->

</head>
<body>
<div class="se-pre-con"></div>
<div class="main">

	<!-- HEADER START -->
	<header class="navbar navbar-custom " id="header">
		<!-- CONTAINER START -->
		<div class="container">
			<!-- ROW START -->
			<div class="row">
				<!-- HEADER LOGO START-->
				<div class="col-md-3 col-xs-6">
					<div class="navbar-header">
						<a class="logo" href="index.php">
							<img  alt="Empire" src="images/logobq.png">
						</a>
					</div>
				</div>
				<!-- HEADER LOGO END-->
				<!-- HEADER ICONS START-->
				<div class="col-md-3 col-xs-6 right-side">
					<div class="header-right-link right-side float-none-xs">
						<ul>
							<li class="search-box">
								<a href="#">
									<span></span>
								</a>
							</li>
							<li class="account-icon">
								<a href="#">
									<span></span>
								</a>
								<div class="header-link-dropdown account-link-dropdown">
									<ul class="link-dropdown-list">
										<li>
											<ul>

												<?php if(!isset($_SESSION['idUser'])) {?>
													<li>
														<?php  require 'login.php'; ?>
													</li>
												<?php } ?>
												<?php if (isset($_SESSION['idUser']) && $_SESSION['phanquyen'] == 1){?>
													<a href="admin/index.php">Admin page</a>
													<br>
													<br>
												<?php } if (isset($_SESSION['idUser'])){ ?>
													<form method="post">
														<button type="submit" name="btnLogout" class="btn btn-danger">Log out</button>
													</form>
												<?php } ?>

											</ul>
										</li>

									</ul>
								</div>
							</li>
							<li class="cart-icon">
                                <?php
                                    if  (isset($_SESSION['idUser'])){
                                        $idU = $_SESSION['idUser'];
                                        $cart =  mysqli_query($conn, "select * from bh_cart where id_user = '$idU'");
                                        $sd = mysqli_num_rows($cart);
                                ?>
								<a href="cart.php">
                      <span>
                        <small class="cart-notification">
                          <?= $sd ?>
                        </small>
                      </span>
								</a>
								<div class="header-link-dropdown cart-dropdown">
									<ul class="cart-list link-dropdown-list">
                                        <?php while ($row_c = mysqli_fetch_assoc($cart)){
	                                        $id_sp = $row_c['id_sanpham'];
	                                        $sanpham = mysqli_query($conn, "select * from bh_sanpham where id = '$id_sp'");
	                                        $row_sp = mysqli_fetch_assoc($sanpham);
                                            ?>
										<li>
											<div class="media">
												<a href="single.php?sp=<?= $row_sp['code'] ?>" class="pull-left">
													<img src="uploads/<?= $row_sp['anhsp'] ?>">
												</a>
												<div class="media-body">
                              <span>
                                <p><?= $row_sp['name'] ?></p>
                              </span>
                                                    <div class="product-qty">
                                                        <label>Số lượng:</label>
                                                        <div class="custom-qty">
															<?= $row_c['soluong'] ?>
                                                        </div>
                                                    </div>
													<p class="cart-price" style="color: #ff0735;"><strong><?= number_format($row_sp['gia'] * $row_c['soluong'], 0 , ',', '.')  ?> VNĐ</strong></p>

												</div>
											</div>
										</li>
                                    <?php } ?>
									</ul>
									<div class="clearfix"></div>
									<div class="mt-20">
										<a href="cart.php" class="btn-color btn">Trang giỏ hàng</a>
									</div>
								</div>
							</li>
                            <?php } ?>
						</ul>
					</div>
					<!-- HEADER TOGGLE BUTTON START-->
					<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
						<i class="fa fa-bars"></i>
					</button>
					<!-- HEADER TOGGLE BUTTON END-->
				</div>
				<!-- HEADER ICONS END-->
				<!-- HEADER MENU START-->
				<div class="col-md-6 col-sm-12 position-s left-side float-none-xs">
					<div class="align-center">
						<div id="menu" class="navbar-collapse collapse">
							<ul class="nav navbar-nav">
								<li class="level">
									<a href="index.php">Trang chủ</a>
								</li>
								<?php
								$qr_phanloai = "select * from bh_phanloai";
								$phanloai = mysqli_query($conn, $qr_phanloai);
								while ($row_pl = mysqli_fetch_assoc($phanloai)){
									?>
									<li class="level">
										<a href="?pl=<?= $row_pl['code'] ?>" class="menu"><?= $row_pl['name'] ?></a>
									</li>
								<?php } ?>

							</ul>
						</div>
					</div>
				</div>
				<!-- HEADER MENU END-->
			</div>
			<!-- ROW END -->
		</div>
		<!-- CONTAINER END -->
	</header>
	<!-- HEADER END -->

	<!-- SEARCH POPUP START -->
	<div class="sidebar-search-wrap">
		<div class="sidebar-table-container">
			<div class="sidebar-align-container">
				<div class="search-closer right-side"></div>
				<div class="search-container">
					<form method="get" id="search-form">
						<input type="text" id="s" class="search-input" name="s" placeholder="Nhập từ khóa tìm kiếm">
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- SEARCH POPUP ENDS -->