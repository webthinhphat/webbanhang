-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2018 at 04:09 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webbanhang-basic`
--

-- --------------------------------------------------------

--
-- Table structure for table `bh_cart`
--

CREATE TABLE `bh_cart` (
  `id` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_sanpham` int(11) DEFAULT NULL,
  `soluong` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bh_phanloai`
--

CREATE TABLE `bh_phanloai` (
  `id` int(11) NOT NULL,
  `name` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bh_phanloai`
--

INSERT INTO `bh_phanloai` (`id`, `name`, `code`) VALUES
(4, 'Son', 'Son'),
(5, 'Dưỡng da', 'Duong-Da');

-- --------------------------------------------------------

--
-- Table structure for table `bh_sanpham`
--

CREATE TABLE `bh_sanpham` (
  `id` int(11) NOT NULL,
  `name` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `anhsp` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `anh2` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `anh3` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gia` int(11) DEFAULT NULL,
  `giacanhtranh` int(11) DEFAULT NULL,
  `mota` text COLLATE utf8_unicode_ci,
  `hangsx` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `xuatxu` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trongluong` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `khuyenmai` int(11) DEFAULT NULL,
  `id_phanloai` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bh_sanpham`
--

INSERT INTO `bh_sanpham` (`id`, `name`, `code`, `anhsp`, `anh2`, `anh3`, `gia`, `giacanhtranh`, `mota`, `hangsx`, `xuatxu`, `trongluong`, `khuyenmai`, `id_phanloai`) VALUES
(1, 'Son Kem Lì Mịn Môi Innisfree Real Fit Matte Liquid', 'Son-Kem-Li-Min-Moi-Innisfree-Real-Fit-Matte-Liquid', '16-04-18-sp-11.png', '16-04-18-sp-', '16-04-18-sp-5.png', 179000, 242000, 'sfndg', 'Innisfree', 'Hàn Quốc', '3.5', 0, 4),
(3, 'Viên sủi trắng da Neuglow C', 'Vien-Sui-Trang-Da-Neuglow-C', '16-04-18-sp-vien-sui-trang-da-neuglow-c.jpg', '16-04-18-sp-', '16-04-18-sp-giap-phap-trang-da-voi-vien-sui-trang-da-newglow-768x461.jpg', 1700000, 1890000, 'NeuGlow C dựa trên sức mạnh hoàn hảo tổng hợp của L -Glutathione (Reduced) khi kết hợp với thành phần Vitamin C theo tỉ lệ 1: 2 sẽ là một sự kết hợp hoàn hảo nhất cho công cuộc làm trắng da. Thành phần 500mg L-Glutathione : 1.000mg Vitamin C sẽ nhanh chóng làm sáng da và châm quá trình gây nên lão hóa da . Vitamin C càng giúp cho L-Glutathione bền vững và tăng khả năng vô hiệu hóa các gốc tự do gây hại làm cho da sạm tối màu và lão hoá.', 'Neuglow C', 'Hàn Quốc', '3.5', 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `bh_slider`
--

CREATE TABLE `bh_slider` (
  `id` int(11) NOT NULL,
  `anh` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bh_slider`
--

INSERT INTO `bh_slider` (`id`, `anh`) VALUES
(21, '16-04-18-sl-banner3.png'),
(22, '16-04-18-sl-banner2.png'),
(23, '16-04-18-sl-banner1.png');

-- --------------------------------------------------------

--
-- Table structure for table `bh_user`
--

CREATE TABLE `bh_user` (
  `id` int(11) NOT NULL,
  `username` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(800) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hoten` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phanquyen` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bh_user`
--

INSERT INTO `bh_user` (`id`, `username`, `password`, `hoten`, `phanquyen`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Nguyen Minh', 1),
(2, 'member', 'aa08769cdcb26674c6706093503ff0a3', 'Member', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bh_cart`
--
ALTER TABLE `bh_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bh_phanloai`
--
ALTER TABLE `bh_phanloai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bh_sanpham`
--
ALTER TABLE `bh_sanpham`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bh_slider`
--
ALTER TABLE `bh_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bh_user`
--
ALTER TABLE `bh_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bh_cart`
--
ALTER TABLE `bh_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bh_phanloai`
--
ALTER TABLE `bh_phanloai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `bh_sanpham`
--
ALTER TABLE `bh_sanpham`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bh_slider`
--
ALTER TABLE `bh_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `bh_user`
--
ALTER TABLE `bh_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
