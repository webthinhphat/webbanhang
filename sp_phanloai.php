<?php
require 'config.php';

$pl = $_GET['pl'];
$qr_pl= "select * from bh_phanloai where code = '$pl'";
$pl = mysqli_query($conn, $qr_pl);
$row_pl = mysqli_fetch_assoc($pl);
$id = $row_pl['id'];


$qr_sp = "select * from bh_sanpham where id_phanloai = '$id'";
$sanpham = mysqli_query($conn, $qr_sp);
while ($row = mysqli_fetch_assoc($sanpham)){
	?>
    <div class="col-lg-3 col-sm-4 col-xs-6 plr-20 mb-30">
        <div class="product-item">
			<?php if ($row['khuyenmai'] == 0){ ?>
                <div class="sale-label"><span>Sale</span></div>
			<?php } ?>
            <div class="product-image">
                <a href="single.php?sp=<?= $row['code'] ?>"></a>
                <img style="width: 285px; height: 393px;" src="uploads/<?= $row['anhsp'] ?>">
            </div>
            <div class="product-item-details align-center">
                <div class="product-item-name"> <a href="single.php?sp=<?= $row['code'] ?>"><?= $row['name'] ?></a> </div>
                <div class="price-box"> <span class="price"><?= number_format($row['gia'], 0, ',', '.')  ?></span> <del class="price old-price"><?= number_format($row['giacanhtranh'], 0, ',', '.')  ?></del> </div>
            </div>
        </div>
    </div>
<?php } ?>