<?php
$qr = "select * from bh_sanpham";
$sanpham = mysqli_query($conn, $qr);
while ($row = mysqli_fetch_assoc($sanpham)){
	?>

		<div class="col-lg-3 col-sm-4 col-xs-6 plr-20 mb-30">
			<div class="product-item">
                <?php if ($row['khuyenmai'] == 1){ ?>
                <div class="sale-label"><span>Sale</span></div>
                <?php } ?>
				<div class="product-image">
					<a href="single.php?sp=<?= $row['code'] ?>"></a>
					<img style="width: 285px; height: 393px;" src="uploads/<?= $row['anhsp'] ?>">
				</div>
				<div class="product-item-details align-center">
					<div class="product-item-name"> <a style="font-weight: bold" href="single.php?sp=<?= $row['code'] ?>"><?= $row['name'] ?></a> </div>
					<div class="price-box"> <span class="price"><?= number_format($row['gia'], 0, ',', '.')  ?> VNĐ</span>
                            <?php if ($row['khuyenmai'] == 1){ ?>
                        <br>
                        <del class="price old-price">
                             <?= number_format($row['giacanhtranh'], 0, ',', '.') ?> VNĐ</del>
                        <?php } ?>
                    </div>
				</div>
			</div>
		</div>

<?php } ?>