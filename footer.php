<!-- FOOTER START -->
<div class="footer">
	<div class="footer-inner">
		<div class="footer-bottom top">
			<div class="container">
				<div class="col-sm-12 align-center">
					<div class="copy-right center-xs">&copy; 2018 by <a href="../" target="_top" class="makerCss">Beaute Pure</a> | All Rights Reserved</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="bottom">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="payment left-side center-xs">
							<ul class="payment_icon">
								<li class="visa"><a><img src="images/pay1.png" alt="Empire"></a></li>
								<li class="discover"><a><img src="images/pay2.png" alt="Empire"></a></li>
								<li class="paypal"><a><img src="images/pay3.png" alt="Empire"></a></li>
								<li class="vindicia"><a><img src="images/pay4.png" alt="Empire"></a></li>
								<li class="atos"><a><img src="images/pay5.png" alt="Empire"></a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="footer_social pt-xs-15 center-xs mt-xs-15 right-side">
							<ul class="social-icon">
								<li><a title="Facebook" class="facebook"><i class="fa fa-facebook"> </i></a></li>
								<li><a title="Twitter" class="twitter"><i class="fa fa-twitter"> </i></a></li>
								<li><a title="Linkedin" class="linkedin"><i class="fa fa-linkedin"> </i></a></li>
								<li><a title="RSS" class="rss"><i class="fa fa-rss"> </i></a></li>
								<li><a title="Pinterest" class="pinterest"><i class="fa fa-pinterest"> </i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="scroll-top">
	<div id="scrollup"></div>
</div>
<!-- FOOTER END -->
</div>
</body>
</html>