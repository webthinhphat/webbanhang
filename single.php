<?php
require 'header.php';
$sp = $_GET['sp'];
$qr = "select * from bh_sanpham where code = '$sp'";
$sanpham = mysqli_query($conn, $qr);
$row = mysqli_fetch_assoc($sanpham);

if (isset($_POST['btnCart'])){
	$id_sanpham =  $row['id'];
	$idUser = $_SESSION['idUser'];
	$soluong = $_POST['qty'];

	$qr_kt = "select * from bh_cart where id_user = '$idUser' and id_sanpham = '$id_sanpham'";
	$check = mysqli_query($conn, $qr_kt);
	$cart = mysqli_fetch_assoc($check);
	if ( mysqli_num_rows($check) == 1){
	    $soluong_up = $soluong + $cart['soluong'];
	    $qr_up = "update bh_cart set soluong = '$soluong_up' where id_user = '$idUser' and id_sanpham = '$id_sanpham'";
	    mysqli_query($conn,$qr_up);
    }else{
		$qr_add= "insert into bh_cart values (null, '$idUser', '$id_sanpham', '$soluong')";
		mysqli_query($conn, $qr_add);
    }
	header('location: cart.php');
}
?>

<!-- BANNER STRAT -->
<div class="banner inner-banner1">
	<div class="container">
		<section class="banner-detail ptb-95">
			<h1 class="banner-title">Product</h1>
			<div class="bread-crumb">
				<ul>
					<li><a href="index.php">Home</a>/</li>
					<li><span>Product</span></li>
				</ul>
			</div>
		</section>
	</div>
</div>
<!-- BANNER END -->

<!-- CONTAIN START -->
<section class="pt-95">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-4 col-sm-4 col-xs-5">
					<div class="fotorama" data-nav="thumbs" data-allowfullscreen="native">
						<a href="#">
							<img src="uploads/<?= $row['anhsp'] ?>" alt="Empire">
						</a>
						<a href="#">
							<img src="uploads/<?= $row['anh2'] ?>" alt="Empire">
						</a>
						<a href="#">
							<img src="uploads/<?= $row['anh3'] ?>" alt="Empire">
						</a>
					</div>
				</div>
				<div class="col-md-8 col-sm-8 col-xs-7">
					<div class="row">
						<div class="col-xs-12">
							<div class="product-detail-main">
								<div class="product-item-details">
									<h1 class="product-item-name" style="font-weight: bold"><?= $row['name'] ?></h1>
									<div class="rating-summary-block">
										<div title="53%" class="rating-result"> <span style="width:53%"></span> </div>
									</div>
									<div class="price-box"> <span class="price"><?= number_format($row['gia'], 0,',','.')?> VNĐ</span>
	                                <?php if ($row['khuyenmai'] == 1){ ?>
                                        <del class="price old-price"><?= number_format($row['giacanhtranh'], 0,',','.')?> VNĐ</del> </div>
                                    <?php } ?>
									<div class="product-info-stock-sku">
										<div>
											<label>Hãng sản xuất: </label>
											<span class="info-deta"><?= $row['hangsx'] ?></span> </div>
										<div>
                                            <label>Xuất xứ: </label>
                                            <span class="info-deta"><?= $row['xuatxu'] ?></span> </div>
                                        <div>
                                            <label>Trọng lượng: </label>
                                            <span class="info-deta"><?= $row['trongluong'] ?> g</span> </div>
                                        <br>
                                        <p><?= _substr($row['mota'], 250)  ?></p>
                                    </div>
                                    <form method="post">
									<div class="mb-20">
										<div class="product-qty">
											<label for="qty">Qty:</label>
											<div class="custom-qty">
												<button onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 1 ) result.value--;return false;" class="reduced items" type="button"> <i class="fa fa-minus"></i> </button>
												<input type="text" class="input-text qty" title="Qty" value="1" maxlength="8" id="qty" name="qty">
												<button onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="increase items" type="button"> <i class="fa fa-plus"></i> </button>
											</div>
										</div>
										<div class="bottom-detail cart-button">
											<ul>
												<li class="pro-cart-icon">

														<input name="btnCart" type="submit" class="btn-color btn" value="Thêm vào giỏ">
													</form>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="ptb-95">
	<div class="container">
		<div class="product-detail-tab">
			<div class="row">
				<div class="col-md-12">
					<div id="tabs">
						<ul class="nav nav-tabs">
							<li><a class="tab-Description selected" title="Description">Mô tả</a></li>
							<li><a class="tab-Reviews" title="Reviews">Reviews</a></li>
						</ul>
					</div>
					<div id="items">
						<div class="tab_content">
							<ul>
								<li>
									<div class="items-Description selected ">
										<div class="Description">
                                            <p><?= $row['mota'] ?></p>
                                        </div>
									</div>
								</li>
								<li>
									<div class="items-Reviews">
										<div class="comments-area">
											<h4>Comments<span>(2)</span></h4>
											<ul class="comment-list mt-30">
												<li>
													<div class="comment-user"> <img src="images/comment-user.jpg" alt="Empire"> </div>
													<div class="comment-detail">
														<div class="user-name">John Doe</div>
														<div class="post-info">
															<ul>
																<li>Fab 11, 2016</li>
																<li><a href="#"><i class="fa fa-reply"></i>Reply</a></li>
															</ul>
														</div>
														<p>Consectetur adipiscing elit integer sit amet augue laoreet maximus nuncac.</p>
													</div>
													<ul class="comment-list child-comment">
														<li>
															<div class="comment-user"> <img src="images/comment-user.jpg" alt="Empire"> </div>
															<div class="comment-detail">
																<div class="user-name">John Doe</div>
																<div class="post-info">
																	<ul>
																		<li>Fab 11, 2016</li>
																		<li><a href="#"><i class="fa fa-reply"></i>Reply</a></li>
																	</ul>
																</div>
																<p>Consectetur adipiscing elit integer sit amet augue laoreet maximus nuncac.</p>
															</div>
														</li>
														<li>
															<div class="comment-user"> <img src="images/comment-user.jpg" alt="Empire"> </div>
															<div class="comment-detail">
																<div class="user-name">John Doe</div>
																<div class="post-info">
																	<ul>
																		<li>Fab 11, 2016</li>
																		<li><a href="#"><i class="fa fa-reply"></i>Reply</a></li>
																	</ul>
																</div>
																<p>Consectetur adipiscing elit integer sit amet augue laoreet maximus nuncac.</p>
															</div>
														</li>
													</ul>
												</li>
												<li>
													<div class="comment-user"> <img src="images/comment-user.jpg" alt="Empire"> </div>
													<div class="comment-detail">
														<div class="user-name">John Doe</div>
														<div class="post-info">
															<ul>
																<li>Fab 11, 2016</li>
																<li><a href="#"><i class="fa fa-reply"></i>Reply</a></li>
															</ul>
														</div>
														<p>Consectetur adipiscing elit integer sit amet augue laoreet maximus nuncac.</p>
													</div>
												</li>
											</ul>
										</div>
										<div class="main-form mt-30">
											<h4>Leave a comments</h4>
											<div class="row mt-30">
												<form >
													<div class="col-sm-4 mb-30">
														<input type="text" placeholder="Name" required>
													</div>
													<div class="col-sm-4 mb-30">
														<input type="email" placeholder="Email" required>
													</div>
													<div class="col-sm-4 mb-30">
														<input type="text" placeholder="Website" required>
													</div>
													<div class="col-xs-12 mb-30">
														<textarea cols="30" rows="3" placeholder="Message" required></textarea>
													</div>
													<div class="col-xs-12 mb-30">
														<button class="btn-color btn" name="submit" type="submit">Submit</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- CONTAINER END -->


<?php require 'footer.php'?>